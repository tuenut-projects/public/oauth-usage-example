FROM python:3.10-slim as builder

SHELL ["/bin/bash", "-c"]

WORKDIR /opt/

RUN python3 -c 'from urllib.request import urlopen; print(urlopen("https://install.python-poetry.org").read().decode())' | \
    python3 - --version 1.4.2
ENV PATH="/root/.local/bin:$PATH"


FROM builder as app

COPY . .

RUN poetry install --no-cache
WORKDIR /opt/oauth_example
