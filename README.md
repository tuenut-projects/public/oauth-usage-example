## How to run example

In `docker-compose.yaml` replace variable value for `SERVICE_OAUTH_CLIENT_SECRET`
in services with values you created in admin panel of Keycloak. 
`SERVICE_OAUTH_CLIENT_ID` should be your service id, you can just create client id 
as it set up in `docker-compose.yaml` for each service.

Then execute curl to service-a and you should get response like below:
```bash
$ curl http://127.0.0.1:28081/user_request
{"message": "This is a protected resource"}
```

### What is going on?
#### Service A
This service accessible for user and user can perform requests to this service
to get some data. But this service will go for data to **service-b**, 
which is unreachable for user network.


1) Handler `oauth_example.app.views.user_request.user_request_handler` receive 
   user request
2) Handler getting token from Keycloak with **service-a** client credentials.
   There is `ClientSession` in aiohttp `app` with url from `SERVICE_OAUTH_URL`
   env var. To request token you need 
   an url (`oauth_example.app.context.oauth.utils.get_url`)  
   and payload (`oauth_example.app.context.oauth.utils.get_payload`)
3) With received token **service-a** performing request to **service-b**, 
   logic placed in `oauth_example.app.services.fetch_data.fetch_from_service`.
4) Then return response from **service-b** to user in Response.

#### Service B
This service is unreachable for user.

1) Service handling requests on url `/service_request` with handler
   `oauth_example.app.views.service_request.service_request_handler`
2) It checks `Authorization` header from request, check value: it should looks like 
   `"Bearer <client_token>"`. If token found the service will validate in in Keycloak
3) Validation request performs in `oauth_example.app.context.oauth.utils.check_token`.
   If response is `"active"`, then service send data in response, otherwise respond is 403.
