import os

from aiohttp import web
from loguru import logger

from oauth_example.app.app_keys import SERVICE_SELF_NAME, SERVICE_NEIGHBOR_HOST
from oauth_example.app.context.oauth.context import oauth_context
from oauth_example.app.views import user_request_handler, service_request_handler


async def get_app() -> web.Application:
    app = web.Application()
    app[SERVICE_SELF_NAME] = os.environ.get("SERVICE_NAME")
    app[SERVICE_NEIGHBOR_HOST] = os.environ.get("SERVICE_NEIGHBOR_HOST")

    app.cleanup_ctx.append(oauth_context)

    app.router.add_get("/user_request", user_request_handler)
    app.router.add_get("/service_request", service_request_handler)

    return app


if __name__ == "__main__":
    logger.info("Starting service-A app.")
    web.run_app(get_app(), port=8080)
