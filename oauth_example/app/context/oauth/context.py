import asyncio
import os

import aiohttp
from aiohttp import web

from oauth_example.app.app_keys import (
    OAUTH_CLIENT_ID,
    OAUTH_CLIENT_SECRET,
    OAUTH_CLIENT_REALM,
    OAUTH_CLIENT_SESSION,
)

OAUTH_URL = os.environ.get("SERVICE_OAUTH_URL")


async def oauth_context(app: web.Application):
    app[OAUTH_CLIENT_ID] = os.environ.get("SERVICE_OAUTH_CLIENT_ID")
    app[OAUTH_CLIENT_SECRET] = os.environ.get("SERVICE_OAUTH_CLIENT_SECRET")
    app[OAUTH_CLIENT_REALM] = os.environ.get("SERVICE_OAUTH_CLIENT_REALM")

    async with aiohttp.ClientSession(OAUTH_URL) as session:
        app[OAUTH_CLIENT_SESSION] = session
        yield

    await asyncio.sleep(0.5)
