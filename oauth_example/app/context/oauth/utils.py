from aiohttp import web
from loguru import logger

from oauth_example.app.app_keys import (
    OAUTH_CLIENT_REALM,
    OAUTH_CLIENT_ID,
    OAUTH_CLIENT_SECRET, OAUTH_CLIENT_SESSION,
)


def get_url(request: web.Request, path: str) -> str:
    realm = request.config_dict[OAUTH_CLIENT_REALM]
    return f"/realms/{realm}/{path}"


def get_payload(request: web.Request, **kwargs) -> dict:
    client_id = request.config_dict[OAUTH_CLIENT_ID]
    client_secret = request.config_dict[OAUTH_CLIENT_SECRET]

    return {**kwargs, "client_id": client_id, "client_secret": client_secret}


async def get_token(request: web.Request) -> str:
    logger.info("Request token from oAuth service.")

    session = request.config_dict[OAUTH_CLIENT_SESSION]

    url = get_url(request, "protocol/openid-connect/token")
    payload = get_payload(request, grant_type="client_credentials")

    async with session.post(url, data=payload) as resp:
        token = await resp.json()
        logger.debug(f"Received token: {token}")
        return token["access_token"]


async def check_token(request: web.Request, token):
    logger.info(f"Check token: {token}")

    session = request.config_dict[OAUTH_CLIENT_SESSION]

    url = get_url(request, "protocol/openid-connect/token/introspect")
    payload = get_payload(request, token=token)

    async with session.post(url, data=payload) as resp:
        data = await resp.json()
        logger.info(f"oAuth introspect response: {data}")

        return data
