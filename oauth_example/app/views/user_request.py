import aiohttp
from aiohttp import web
from loguru import logger

from oauth_example.app.services.fetch_data import fetch_from_service
from oauth_example.app.context.oauth.utils import get_token


async def user_request_handler(request):
    logger.info("Get user request.")

    token = await get_token(request)
    logger.info(f"Token received: {token}")

    async with aiohttp.ClientSession() as session:
        data = await fetch_from_service(request, session, token)
        logger.info(f"Data received: {data}")
        return web.json_response(data)
