from aiohttp import web
from loguru import logger

from oauth_example.app.context.oauth.utils import check_token


async def service_request_handler(request):
    logger.info("Received request from another service.")

    auth_header = request.headers.get("Authorization")
    if not auth_header or not auth_header.startswith("Bearer "):
        logger.warning("Request unauthorized.")
        return web.json_response({"error": "Unauthorized"}, status=401)

    token = auth_header.split()[1]
    introspection_response = await check_token(request, token)

    if not introspection_response.get("active"):
        return web.json_response({"error": "Unauthorized"}, status=401)

    logger.info("Request successful.")
    return web.json_response({"message": "This is a protected resource"})
