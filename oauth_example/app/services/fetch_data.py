from aiohttp import web
from loguru import logger

from oauth_example.app.app_keys import SERVICE_NEIGHBOR_HOST


async def fetch_from_service(request: web.Request, session, token):
    logger.info("Fetching data from service-b.")
    service_host = request.config_dict[SERVICE_NEIGHBOR_HOST]

    url = f"{service_host}/service_request"
    headers = {"Authorization": f"Bearer {token}"}

    async with session.get(url, headers=headers) as resp:
        return await resp.json()
