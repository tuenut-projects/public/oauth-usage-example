import aiohttp
from aiohttp import web

OAUTH_CLIENT_ID = web.AppKey("oauth.client_id", str)
OAUTH_CLIENT_SECRET = web.AppKey("oauth.client_secret", str)
OAUTH_CLIENT_REALM = web.AppKey("oauth.client_realm", str)
OAUTH_CLIENT_SESSION = web.AppKey("oauth.session", aiohttp.ClientSession)

SERVICE_SELF_NAME = web.AppKey("service.name", str)
SERVICE_NEIGHBOR_HOST = web.AppKey("service.neighbor_host", str)
